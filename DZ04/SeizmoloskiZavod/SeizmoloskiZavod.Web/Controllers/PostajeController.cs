﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class PostajeController : Controller
    {
        // GET: Postaje
        public ActionResult Index()
        {
            return View(Postaja.Get());
        }

        // GET: Postaje/Details/5
        public ActionResult Details(int id)
        {
                Postaja postaja = Postaja.Get(id);
                var seizmografi = Seizmograf.Get().ToList();
                        /*.AsNoTracking()
                        .ToList();*/
                ViewData["seizmografi"] = seizmografi;
                return View(postaja);
            
        }

        // GET: Postaje/Create
        public ActionResult Create()
        {
            return View(Postaja.New());
        }

        // POST: Postaje/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(string NazPostaje, string MjestoPostaje)
        {
            Postaja postaja = Postaja.New();
            try
            {
                postaja.NazPostaje = NazPostaje;
                postaja.MjestoPostaje = MjestoPostaje;
                postaja = postaja.Save();
                return RedirectToAction("Details", new { id = postaja.IdPostaje });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (postaja.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in postaja.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(postaja);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(postaja);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(postaja);
            }
        }

        //
        // GET: /Postaja/Edit/5
        public ActionResult Edit(int id)
        {
            Postaja postaja = Postaja.Get(id);
            return View(postaja);
        }

        //
        // POST: /Postaja/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string NazPostaje, string MjestoPostaje)
        {
            Postaja postaja = null;
            try
            {
                postaja = Postaja.Get(id);
                postaja.NazPostaje = NazPostaje;
                postaja.MjestoPostaje = MjestoPostaje;
                postaja.Save();
                return RedirectToAction("Details", new { id = postaja.IdPostaje });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (postaja.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in postaja.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(postaja);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(postaja);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(postaja);
            }
        }
        
        // POST: Postaje/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                Postaja.Delete(id);
            }
            catch (Exception ex)
            {
                TempData["Pogreska"] = ex.Message;
            }
            return RedirectToAction("Index");
        }
    }
}
