﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class SeizmogramiController : Controller
    {
        // POST: Seizmogrami/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            int idSeizmografa = Seizmogram.Get(id).IdSeizmografa;

            try
            {
                Seizmogram.Delete(id);
            }
            catch (Exception ex)
            {
                TempData["Pogreska"] = ex.Message;
            }
            return RedirectToAction("../Seizmografi/Details", new { id = idSeizmografa });
        }
    }
}
