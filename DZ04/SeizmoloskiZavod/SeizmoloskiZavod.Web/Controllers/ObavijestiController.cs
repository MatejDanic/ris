﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class ObavijestiController : Controller
    {
        public ActionResult Index()
        {
            var tipovi = TipObavijesti.Get().ToList();
            ViewData["tipovi"] = tipovi;
            var zaposlenici = Zaposlenik.Get().ToList();
            ViewData["zaposlenici"] = zaposlenici;
            return View(Obavijest.Get());
        }

        // GET: Obavijesti/Create
        public ActionResult Create()
        {
            ViewBag.IdTipovaList = new SelectList(TipObavijesti.Get(), "IdTipa", "NazTipa");
            ViewBag.IdZaposlenikaList = new SelectList(Zaposlenik.Get(), "IdZaposlenika", "KorisnickoImeZaposlenika");

            return View(Obavijest.New());
        }

        // POST: Obavijesti/Create
        [HttpPost]
        public ActionResult Create(string NaslovObavijesti, string SadrzajObavijesti, int IdTipaObavijesti, int IdZaposlenikaObavijesti)
        {

            Obavijest obavijest = Obavijest.New();
            try
            {
                obavijest.IdTipaObavijesti = IdTipaObavijesti;
                obavijest.IdZaposlenikaObavijesti = IdZaposlenikaObavijesti;
                obavijest.NaslovObavijesti = NaslovObavijesti;
                obavijest.SadrzajObavijesti = SadrzajObavijesti;
                obavijest.DatumVrijemeObavijesti = DateTime.Now;

                obavijest = obavijest.Save();
                return RedirectToAction("Index");
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (obavijest.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in obavijest.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(obavijest);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(obavijest);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(obavijest);
            }
        }

        // GET: Obavijesti/Edit/5
        public ActionResult Edit(int id)
        {

            Obavijest obavijest = Obavijest.Get(id);
            ViewBag.IdTipovaList = new SelectList(TipObavijesti.Get(), "IdTipa", "NazTipa");
            ViewBag.IdZaposlenikaList = new SelectList(Zaposlenik.Get(), "IdZaposlenika", "KorisnickoImeZaposlenika");

            return View(obavijest);
        }

        // POST: Obavijesti/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string NaslovObavijesti, string SadrzajObavijesti, int IdTipaObavijesti, int IdZaposlenikaObavijesti)
        {
            Obavijest obavijest = null;
            try
            {
                obavijest = Obavijest.Get(id);
                obavijest.NaslovObavijesti = NaslovObavijesti;
                obavijest.SadrzajObavijesti = SadrzajObavijesti;
                obavijest.IdTipaObavijesti = IdTipaObavijesti;
                obavijest.IdZaposlenikaObavijesti = IdZaposlenikaObavijesti;
                obavijest.DatumVrijemeObavijesti = DateTime.Now; ;
                obavijest.Save();
                return RedirectToAction("Index");
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (obavijest.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in obavijest.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(obavijest);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(obavijest);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(obavijest);
            }
        }

        // POST: Obavijesti/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                Obavijest.Delete(id);
            }
            catch (Exception ex)
            {
                TempData["Pogreska"] = ex.Message;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public string PromijeniTip(int IdObavijesti, int IdTipa)
        {
            try
            {
                Obavijest obavijest = Obavijest.Get(IdObavijesti);
                obavijest.IdTipaObavijesti = IdTipa;
                obavijest.Save();
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
