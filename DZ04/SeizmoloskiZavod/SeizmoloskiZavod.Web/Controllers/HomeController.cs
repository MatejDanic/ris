﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var tipovi = TipObavijesti.Get().ToList();
            ViewData["tipovi"] = tipovi;
            return View(Obavijest.Get());
        }
    }
}