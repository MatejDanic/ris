﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Zaposlenik : BusinessBase<Zaposlenik>
    {
        private static PropertyInfo<int> IdZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<int>(Reflector.GetPropertyName<Zaposlenik>(x => x.IdZaposlenika)));
        public int IdZaposlenika
        {
            get { return GetProperty(IdZaposlenikaProperty); }
            set { SetProperty(IdZaposlenikaProperty, value); }
        }

        private static PropertyInfo<int> IdUlogeProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<int>(Reflector.GetPropertyName<Zaposlenik>(x => x.IdUlogeZaposlenika)));
        public int IdUlogeZaposlenika
        {
            get { return GetProperty(IdUlogeProperty); }
            set { SetProperty(IdUlogeProperty, value); }
        }


        private static PropertyInfo<string> ImeZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.ImeZaposlenika)));
        public string ImeZaposlenika
        {
            get { return GetProperty(ImeZaposlenikaProperty); }
            set { SetProperty(ImeZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> PrezimeZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.PrezimeZaposlenika)));
        public string PrezimeZaposlenika
        {
            get { return GetProperty(PrezimeZaposlenikaProperty); }
            set { SetProperty(PrezimeZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> OIBZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.OIBZaposlenika)));
        public string OIBZaposlenika
        {
            get { return GetProperty(OIBZaposlenikaProperty); }
            set { SetProperty(OIBZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> KorisnickoImeZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.KorisnickoImeZaposlenika)));
        public string KorisnickoImeZaposlenika
        {
            get { return GetProperty(KorisnickoImeZaposlenikaProperty); }
            set { SetProperty(KorisnickoImeZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> LozinkaHashZaposlenikaProperty =
      RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.LozinkaHashZaposlenika)));
        public string LozinkaHashZaposlenika
        {
            get { return GetProperty(LozinkaHashZaposlenikaProperty); }
            set { SetProperty(LozinkaHashZaposlenikaProperty, value); }
        }

        private static PropertyInfo<string> UlogaZaposlenikaProperty =
     RegisterProperty(typeof(Zaposlenik), new PropertyInfo<string>(Reflector.GetPropertyName<Zaposlenik>(x => x.UlogaZaposlenika)));
        public string UlogaZaposlenika
        {
            get { return GetProperty(UlogaZaposlenikaProperty); }
            set { SetProperty(UlogaZaposlenikaProperty, value); }
        }

        public static Zaposlenik New()
        {
            return DataPortal.Create<Zaposlenik>();
        }

        public static Zaposlenik Get(int idZaposlenika)
        {
            return DataPortal.Fetch<Zaposlenik>(new SingleCriteria<Zaposlenik, int>(idZaposlenika));
        }

        public static List<Zaposlenik> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from z in ctx.DataContext.Zaposlenici
                           select new Zaposlenik { IdZaposlenika = z.Id, OIBZaposlenika = z.OIB, IdUlogeZaposlenika = z.IdUloge, ImeZaposlenika = z.Ime, PrezimeZaposlenika = z.Prezime, KorisnickoImeZaposlenika = z.KorisnickoIme, LozinkaHashZaposlenika = z.LozinkaHash };
                return data.ToList();
            }

        }
        public static void Delete(int idZaposlenika)
        {
            DataPortal.Delete(new SingleCriteria<Zaposlenik, int>(idZaposlenika));
        }

        private void DataPortal_Fetch(SingleCriteria<Zaposlenik, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Zaposlenici.Find(criteria.Value);

                LoadProperty(IdZaposlenikaProperty, data.Id);
                LoadProperty(IdUlogeProperty, data.IdUloge);
                LoadProperty(OIBZaposlenikaProperty, data.OIB);
                LoadProperty(ImeZaposlenikaProperty, data.Ime);
                LoadProperty(PrezimeZaposlenikaProperty, data.Prezime);
                LoadProperty(KorisnickoImeZaposlenikaProperty, data.KorisnickoIme);
                LoadProperty(LozinkaHashZaposlenikaProperty, data.LozinkaHash);
                LoadProperty(UlogaZaposlenikaProperty, Uloga.Get(data.IdUloge).NazUloge);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Zaposlenik z = new Dal.Zaposlenik
                {
                    Ime = ImeZaposlenika,
                    Prezime = PrezimeZaposlenika,
                    KorisnickoIme = KorisnickoImeZaposlenika,
                    LozinkaHash = LozinkaHashZaposlenika,
                    OIB = OIBZaposlenika,
                    IdUloge = IdUlogeZaposlenika
                };

                ctx.DataContext.Zaposlenici.Add(z);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdZaposlenikaProperty, z.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Zaposlenik z = ctx.DataContext.Zaposlenici.Find(this.IdZaposlenika);

                z.IdUloge = IdUlogeZaposlenika;
                z.Ime = ImeZaposlenika;
                z.Prezime = PrezimeZaposlenika;
                z.OIB = OIBZaposlenika;
                z.KorisnickoIme = KorisnickoImeZaposlenika;
                z.LozinkaHash = LozinkaHashZaposlenika;
                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Zaposlenik, int>(ReadProperty(IdZaposlenikaProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Zaposlenik, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var z = ctx.DataContext.Zaposlenici.Find(criteria.Value);
                if (z != null)
                {
                    ctx.DataContext.Zaposlenici.Remove(z);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
