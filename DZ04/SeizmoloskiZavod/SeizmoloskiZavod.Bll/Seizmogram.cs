﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Seizmogram : BusinessBase<Seizmogram>
    {
        private static PropertyInfo<int> IdSeizmogramaProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<int>(Reflector.GetPropertyName<Seizmogram>(x => x.IdSeizmograma)));
        public int IdSeizmograma
        {
            get { return GetProperty(IdSeizmogramaProperty); }
            set { SetProperty(IdSeizmogramaProperty, value); }
        }

        private static PropertyInfo<int> IdSeizmografaProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<int>(Reflector.GetPropertyName<Seizmogram>(x => x.IdSeizmografa)));
        public int IdSeizmografa
        {
            get { return GetProperty(IdSeizmografaProperty); }
            set { SetProperty(IdSeizmografaProperty, value); }
        }

        private static PropertyInfo<DateTime> DatumVrijemeProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<DateTime>(Reflector.GetPropertyName<Seizmogram>(x => x.DatumVrijemeSeizmograma)));
        public DateTime DatumVrijemeSeizmograma
        {
            get { return GetProperty(DatumVrijemeProperty); }
            set { SetProperty(DatumVrijemeProperty, value); }
        }


        private static PropertyInfo<double> PomakSJProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmogram>(x => x.PomakSJSeizmograma)));
        public double PomakSJSeizmograma
        {
            get { return GetProperty(PomakSJProperty); }
            set { SetProperty(PomakSJProperty, value); }
        }

        private static PropertyInfo<double> PomakIZProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmogram>(x => x.PomakIZSeizmograma)));
        public double PomakIZSeizmograma
        {
            get { return GetProperty(PomakIZProperty); }
            set { SetProperty(PomakIZProperty, value); }
        }

        private static PropertyInfo<double> PomakVertProperty =
      RegisterProperty(typeof(Seizmogram), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmogram>(x => x.PomakVertSeizmograma)));
        public double PomakVertSeizmograma
        {
            get { return GetProperty(PomakVertProperty); }
            set { SetProperty(PomakVertProperty, value); }
        }

        public static Seizmogram New()
        {
            return DataPortal.Create<Seizmogram>();
        }

        public static Seizmogram Get(int idSeizmograma)
        {
            return DataPortal.Fetch<Seizmogram>(new SingleCriteria<Seizmogram, int>(idSeizmograma));
        }

        public static List<Seizmogram> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from s in ctx.DataContext.Seizmogrami
                           select new Seizmogram {DatumVrijemeSeizmograma = s.DatumVrijeme, IdSeizmograma = s.Id, IdSeizmografa = s.IdSeizmografa, PomakIZSeizmograma = s.PomakIZ, PomakSJSeizmograma = s.PomakSJ, PomakVertSeizmograma = s.PomakVert };
                return data.ToList();
            }
        }

        public static void Delete(int idSeizmograma)
        {
            DataPortal.Delete(new SingleCriteria<Seizmogram, int>(idSeizmograma));
        }

        private void DataPortal_Fetch(SingleCriteria<Seizmogram, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Seizmogrami.Find(criteria.Value);

                LoadProperty(IdSeizmogramaProperty, data.Id);
                LoadProperty(IdSeizmografaProperty, data.IdSeizmografa);
                LoadProperty(PomakIZProperty, data.PomakIZ);
                LoadProperty(PomakSJProperty, data.PomakSJ);
                LoadProperty(PomakVertProperty, data.PomakVert);
                LoadProperty(DatumVrijemeProperty, data.DatumVrijeme);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Seizmogram s = new Dal.Seizmogram
                {
                    IdSeizmografa = IdSeizmografa,
                    PomakIZ = PomakIZSeizmograma,
                    PomakSJ = PomakSJSeizmograma,
                    PomakVert = PomakVertSeizmograma,
                    DatumVrijeme = DatumVrijemeSeizmograma
                };

                ctx.DataContext.Seizmogrami.Add(s);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdSeizmogramaProperty, s.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Seizmogram, int>(ReadProperty(IdSeizmogramaProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Seizmogram, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var s = ctx.DataContext.Seizmogrami.Find(criteria.Value);
                if (s != null)
                {
                    ctx.DataContext.Seizmogrami.Remove(s);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
