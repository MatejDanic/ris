﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.Web.Controllers
{
    public class SeizmografiController : Controller
    {
        // GET: Seizmografi
        public ActionResult Index()
        {
            var postaje = Postaja.Get().ToList();
            ViewData["postaje"] = postaje;
            return View(Seizmograf.Get());
        }

        // GET: Seizmografi/Details/5
        public ActionResult Details(int id)
        {
            var seizmogrami = Seizmogram.Get().ToList();
            ViewData["seizmogrami"] = seizmogrami;
            return View(Seizmograf.Get(id));
        }

        // GET: Seizmografi/Create
        public ActionResult Create()
        {
            ViewBag.IdPostajeList = new SelectList(Postaja.Get(), "IdPostaje", "NazPostaje");
            return View(Seizmograf.New());
        }

        // POST: Seizmografi/Create
        [HttpPost]
        public ActionResult Create(string NazSeizmografa, int IdPostajeSeizmografa, double VisinaSeizmografa, double LatitudaSeizmografa, double LongitudaSeizmografa)
        {

            Seizmograf seizmograf = Seizmograf.New();
            try
            {
                //ViewBag.IdPostajeSeizmografa = (new SelectList(Postaja.Get(), "IdPostaje", "NazPostaje"));

                seizmograf.IdPostajeSeizmografa = IdPostajeSeizmografa;
                seizmograf.VisinaSeizmografa = VisinaSeizmografa;
                seizmograf.LatitudaSeizmografa = LatitudaSeizmografa;
                seizmograf.LongitudaSeizmografa = LongitudaSeizmografa;
                seizmograf.NazSeizmografa = NazSeizmografa;

                seizmograf = seizmograf.Save();
                return RedirectToAction("Details", new { id = seizmograf.IdSeizmografa });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (seizmograf.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in seizmograf.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(seizmograf);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(seizmograf);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(seizmograf);
            }
        }

        public ActionResult Mjerenje()
        {
            return View(SeizmografInfoList.Get());
        }

        [HttpPost]
        public ActionResult Mjerenje(FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                foreach (Seizmograf s in Seizmograf.Get())
                {
                    if (formCollection[s.IdSeizmografa.ToString()] != null)
                    {
                        Seizmogram seizmogram = Seizmogram.New();
                        seizmogram.IdSeizmografa = s.IdSeizmografa;
                        seizmogram.PomakIZSeizmograma = Math.Round(VrijednostiMjerenja.GetRandomNumber(0.00001, 0.50000), 5, MidpointRounding.AwayFromZero);
                        seizmogram.PomakSJSeizmograma = Math.Round(VrijednostiMjerenja.GetRandomNumber(0.00001, 0.50000), 5, MidpointRounding.AwayFromZero);
                        seizmogram.PomakVertSeizmograma = Math.Round(VrijednostiMjerenja.GetRandomNumber(0.00001, 0.50000), 5, MidpointRounding.AwayFromZero);
                        seizmogram.DatumVrijemeSeizmograma = DateTime.Now;

                        seizmogram = seizmogram.Save();
                    }
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Seizmografi/Edit/5
        public ActionResult Edit(int id)
        {

            Seizmograf seizmograf = Seizmograf.Get(id);
            ViewBag.IdPostajeList = new SelectList(Postaja.Get(), "IdPostaje", "NazPostaje");

            return View(seizmograf);
        }

        // POST: Seizmografi/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, int IdPostajeSeizmografa, double VisinaSeizmografa, double LatitudaSeizmografa, double LongitudaSeizmografa, string NazSeizmografa)
        {
            Seizmograf seizmograf = null;
            try
            {
                seizmograf = Seizmograf.Get(id);
                seizmograf.IdPostajeSeizmografa = IdPostajeSeizmografa;
                seizmograf.VisinaSeizmografa = VisinaSeizmografa;
                seizmograf.LatitudaSeizmografa = LatitudaSeizmografa;
                seizmograf.LongitudaSeizmografa = LongitudaSeizmografa;
                seizmograf.NazSeizmografa = NazSeizmografa;
                seizmograf.Save();
                return RedirectToAction("Details", new { id = seizmograf.IdSeizmografa });
            }
            catch (Csla.Validation.ValidationException ex)
            {
                ViewBag.Pogreska = ex.Message;
                if (seizmograf.BrokenRulesCollection.Count > 0)
                {
                    List<string> errors = new List<string>();
                    foreach (Csla.Validation.BrokenRule rule in seizmograf.BrokenRulesCollection)
                    {
                        errors.Add(string.Format("{0}: {1}", rule.Property, rule.Description));
                        ModelState.AddModelError(rule.Property, rule.Description);
                    }
                    ViewBag.ErrorsList = errors;
                }
                return View(seizmograf);
            }
            catch (Csla.DataPortalException ex)
            {
                ViewBag.Pogreska = ex.BusinessException.Message;
                return View(seizmograf);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(seizmograf);
            }
        }

        /*[HttpGet]
        public ActionResult EditPartial(int id)
        {
            Seizmograf seizmograf = null;

            ViewBag.IdPostajeList = new SelectList(Postaja.Get(), "IdPostaje", "NazPostaje");
            try
            {
                seizmograf = Seizmograf.Get(id);
            }
            catch (Exception ex)
            {
                ViewBag.Pogreska = ex.Message;
                return View(seizmograf);
            }
            if (seizmograf != null)
            {

                return PartialView(seizmograf);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult EditPartial(Seizmograf seizmograf)
        {
            if (seizmograf == null)
            {
                return null;
            }
            ViewBag.IdPostajeList = new SelectList(Postaja.Get(), "IdPostaje", "NazPostaje");
            if (ModelState.IsValid)
            {
                try
                {
                    seizmograf.Save();
                    return StatusCode(302, Url.Action(nameof(Row), new { id = seizmograf.IdSeizmografa }));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(seizmograf);
                }
            }
            else
            {
                return PartialView(seizmograf);
            }
        }*/

        // POST: Seizmografi/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            int idPostaje = Seizmograf.Get(id).IdPostajeSeizmografa;

            try
            {
                Seizmograf.Delete(id);
            }
            catch (Exception ex)
            {
                TempData["Pogreska"] = ex.Message;
                return RedirectToAction("/Details", new { id = id });
            }
            return RedirectToAction("../Postaje/Details", new { id = idPostaje });
        }
    }
}
