﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Seizmograf : BusinessBase<Seizmograf>
    {

        private static PropertyInfo<int> IdSeizmografaProperty =
      RegisterProperty(typeof(Seizmograf), new PropertyInfo<int>(Reflector.GetPropertyName<Seizmograf>(x => x.IdSeizmografa)));
        public int IdSeizmografa
        {
            get { return GetProperty(IdSeizmografaProperty); }
            set { SetProperty(IdSeizmografaProperty, value); }
        }

        private static PropertyInfo<int> IdPostajeProperty =
      RegisterProperty(typeof(Seizmograf), new PropertyInfo<int>(Reflector.GetPropertyName<Seizmograf>(x => x.IdPostajeSeizmografa)));
        public int IdPostajeSeizmografa
        {
            get { return GetProperty(IdPostajeProperty); }
            set { SetProperty(IdPostajeProperty, value); }
        }


        private static PropertyInfo<double> VisinaProperty =
      RegisterProperty(typeof(Seizmograf), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmograf>(x => x.VisinaSeizmografa)));
        public double VisinaSeizmografa
        {
            get { return GetProperty(VisinaProperty); }
            set { SetProperty(VisinaProperty, value); }
        }

        private static PropertyInfo<double> LatitudaProperty =
     RegisterProperty(typeof(Seizmograf), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmograf>(x => x.LatitudaSeizmografa)));
        public double LatitudaSeizmografa
        {
            get { return GetProperty(LatitudaProperty); }
            set { SetProperty(LatitudaProperty, value); }
        }

        private static PropertyInfo<double> LongitudaProperty =
     RegisterProperty(typeof(Seizmograf), new PropertyInfo<double>(Reflector.GetPropertyName<Seizmograf>(x => x.LongitudaSeizmografa)));
        public double LongitudaSeizmografa
        {
            get { return GetProperty(LongitudaProperty); }
            set { SetProperty(LongitudaProperty, value); }
        }

        private static PropertyInfo<string> PostajaSeizmografaProperty =
     RegisterProperty(typeof(Seizmograf), new PropertyInfo<string>(Reflector.GetPropertyName<Seizmograf>(x => x.PostajaSeizmografa)));
        public string PostajaSeizmografa
        {
            get { return GetProperty(PostajaSeizmografaProperty); }
            set { SetProperty(PostajaSeizmografaProperty, value); }
        }

        private static PropertyInfo<string> NazSeizmografaProperty =
     RegisterProperty(typeof(Seizmograf), new PropertyInfo<string>(Reflector.GetPropertyName<Seizmograf>(x => x.NazSeizmografa)));
        public string NazSeizmografa
        {
            get { return GetProperty(NazSeizmografaProperty); }
            set { SetProperty(NazSeizmografaProperty, value); }
        }

        public static Seizmograf New()
        {
            return DataPortal.Create<Seizmograf>();
        }

        public static Seizmograf Get(int idSeizmografa)
        {
            return DataPortal.Fetch<Seizmograf>(new SingleCriteria<Seizmograf, int>(idSeizmografa));
        }

        public static List<Seizmograf> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from s in ctx.DataContext.Seizmografi
                           select new Seizmograf { IdSeizmografa = s.Id, IdPostajeSeizmografa = s.IdPostaje, VisinaSeizmografa = s.Visina, LatitudaSeizmografa = s.Latituda, LongitudaSeizmografa = s.Longituda, NazSeizmografa = s.Naziv };
                return data.ToList();
            }

        }
        public static void Delete(int idSeizmografa)
        {
            DataPortal.Delete(new SingleCriteria<Seizmograf, int>(idSeizmografa));
        }

        private void DataPortal_Fetch(SingleCriteria<Seizmograf, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Seizmografi.Find(criteria.Value);

                LoadProperty(IdSeizmografaProperty, data.Id);
                LoadProperty(VisinaProperty, data.Visina);
                LoadProperty(LatitudaProperty, data.Latituda);
                LoadProperty(LongitudaProperty, data.Longituda);
                LoadProperty(IdPostajeProperty, data.IdPostaje);
                LoadProperty(NazSeizmografaProperty, data.Naziv);
                LoadProperty(PostajaSeizmografaProperty, Postaja.Get(data.IdPostaje).NazPostaje);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Seizmograf s = new Dal.Seizmograf
                {
                    Visina = VisinaSeizmografa,
                    Latituda = LatitudaSeizmografa,
                    Longituda = LongitudaSeizmografa,
                    IdPostaje = IdPostajeSeizmografa,
                    Naziv = NazSeizmografa
                };

                ctx.DataContext.Seizmografi.Add(s);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdSeizmografaProperty, s.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Seizmograf s = ctx.DataContext.Seizmografi.Find(this.IdSeizmografa);

                s.IdPostaje = IdPostajeSeizmografa;
                s.Visina = VisinaSeizmografa;
                s.Latituda = LatitudaSeizmografa;
                s.Longituda = LongitudaSeizmografa;
                s.Naziv = NazSeizmografa;
                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Seizmograf, int>(ReadProperty(IdSeizmografaProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Seizmograf, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var s = ctx.DataContext.Seizmografi.Find(criteria.Value);
                if (s != null)
                {
                    ctx.DataContext.Seizmografi.Remove(s);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
