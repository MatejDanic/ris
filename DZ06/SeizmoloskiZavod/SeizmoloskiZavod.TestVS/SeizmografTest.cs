﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeizmoloskiZavod.Bll;

namespace SeizmoloskiZavod.TestVS
{
    [TestClass]
    public class SeizmografTest
    {
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        private Postaja postaja;
        private string nazivSeizmografa;
        private double visina, latituda, longituda;
        private Seizmograf seizmograf;

        [TestInitialize()]
        public void TestInitialize()
        {
            postaja = new Postaja("MjestoTest", "NazivTest");
        
            nazivSeizmografa = "NZV01";
            visina = 13.534;
            latituda = 46.472;
            longituda = 17.263;
        }

        [TestMethod]
        public void TestMethod1()
        {
            seizmograf = new Seizmograf(postaja, nazivSeizmografa, visina, latituda, longituda);
            //Console.WriteLine(seizmograf.IdSeizmografa);

            Console.WriteLine(seizmograf.IdPostajeSeizmografa + " " + postaja.IdPostaje);

            Assert.AreEqual(seizmograf.IdPostajeSeizmografa, postaja.IdPostaje);
        }

        [TestMethod]
        public void TestMethod2()
        {
            double visinaTest = 0;
            seizmograf = new Seizmograf(postaja, nazivSeizmografa, visinaTest, latituda, longituda);
            double ocekivano = 0;
            double stvarno;
            seizmograf.VisinaSeizmografa = ocekivano;
            stvarno = seizmograf.VisinaSeizmografa;
            Assert.AreEqual(ocekivano, stvarno);
        }


        [TestMethod]
        public void TestMethod3()
        {
            nazivSeizmografa = "VELIKINAZIV";
            seizmograf = new Seizmograf(postaja, nazivSeizmografa, visina, latituda, longituda);            Console.WriteLine(seizmograf.IdPostajeSeizmografa + " " + postaja.IdPostaje);
            Console.WriteLine(seizmograf.NazSeizmografa + " " + nazivSeizmografa);
            Assert.IsFalse(seizmograf.IdSeizmografa == 0);
        }

        [TestMethod]
        public void TestMethod4()
        {
            nazivSeizmografa = "NAZIV";
            seizmograf = new Seizmograf(postaja, nazivSeizmografa, visina, latituda, longituda); Console.WriteLine(seizmograf.IdPostajeSeizmografa + " " + postaja.IdPostaje);
            Console.WriteLine(seizmograf.NazSeizmografa + " " + nazivSeizmografa);
            Assert.IsFalse(seizmograf.IdSeizmografa == 0);
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            seizmograf.Remove();
            postaja.Remove();
        }
    }
}
