﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csla;
using System.Reflection;

namespace SeizmoloskiZavod.Bll
{
    [Serializable()]
    public class Uloga : BusinessBase<Uloga>
    {
        private Uloga()
        {
        }

        private static PropertyInfo<int> IdUlogeProperty =
      RegisterProperty(typeof(Uloga), new PropertyInfo<int>(Reflector.GetPropertyName<Uloga>(x => x.IdUloge)));
        public int IdUloge
        {
            get { return GetProperty(IdUlogeProperty); }
            set { SetProperty(IdUlogeProperty, value); }
        }


        private static PropertyInfo<string> NazUlogeProperty =
      RegisterProperty(typeof(Uloga), new PropertyInfo<string>(Reflector.GetPropertyName<Uloga>(x => x.NazUloge)));
        public string NazUloge
        {
            get { return GetProperty(NazUlogeProperty); }
            set { SetProperty(NazUlogeProperty, value); }
        }

        
        public static Uloga New()
        {
            return DataPortal.Create<Uloga>();
        }

        public static Uloga Get(int idUloge)
        {
            return DataPortal.Fetch<Uloga>(new SingleCriteria<Uloga, int>(idUloge));
        }

        public static List<Uloga> Get()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = from u in ctx.DataContext.Uloge
                           select new Uloga { IdUloge = u.Id, NazUloge = u.Naziv };

                return data.ToList();
            }

        }
        public static void Delete(int idUloge)
        {
            DataPortal.Delete(new SingleCriteria<Uloga, int>(idUloge));
        }

        private void DataPortal_Fetch(SingleCriteria<Uloga, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var data = ctx.DataContext.Uloge.Find(criteria.Value);

                LoadProperty(IdUlogeProperty, data.Id);
                LoadProperty(NazUlogeProperty, data.Naziv);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Uloga u = new Dal.Uloga
                {
                    Naziv = NazUloge
                };

                ctx.DataContext.Uloge.Add(u);
                ctx.DataContext.SaveChanges();

                LoadProperty(IdUlogeProperty, u.Id);
                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                Dal.Uloga u = ctx.DataContext.Uloge.Find(this.IdUloge);

                u.Naziv = NazUloge;

                ctx.DataContext.SaveChanges();

                FieldManager.UpdateChildren(this);
            }
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new SingleCriteria<Uloga, int>(ReadProperty(IdUlogeProperty)));
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        private void DataPortal_Delete(SingleCriteria<Uloga, int> criteria)
        {
            using (var ctx = Dal.ContextManager<Dal.ModelDataContext>.GetManager(Dal.Database.ModelConnectionString))
            {
                var u = ctx.DataContext.Uloge.Find(criteria.Value);
                if (u != null)
                {
                    ctx.DataContext.Uloge.Remove(u);
                    ctx.DataContext.SaveChanges();
                }
            }
        }
    }
}
